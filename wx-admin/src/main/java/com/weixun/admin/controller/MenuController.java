package com.weixun.admin.controller;

import com.jfinal.core.Controller;
import com.jfinal.json.JFinalJson;
import com.jfinal.plugin.activerecord.Record;
import com.weixun.comm.model.vo.Staff;
import com.weixun.admin.model.vo.TreeView;
import com.weixun.comm.model.vo.Ztree;
import com.weixun.model.SysMenu;
import com.weixun.admin.model.vo.Menu;
import com.weixun.admin.service.MenuService;
import com.weixun.utils.ajax.AjaxMsg;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myd on 2017/5/10.
 */
public class MenuController extends Controller {

    static MenuService menuService = new MenuService();
    public void index(){}

    /**
     * 获取treegrid树形列表数据
     */
    public void menulist(){

        Menu menu = new Menu();
        menu.setId(0);
        List<Menu> menu_parents = this.tree(menu);
        renderJson(JFinalJson.getJson().toJson(menu_parents));
    }

    /**
     * 获取登录后的树形菜单
     */
    public void login_menu_list()
    {
//        String role_pk = getPara("role_pk");
        Staff staff = (Staff) getSession().getAttribute("loginUser");
        String role_pk = staff.getFk_roles_pk();
        TreeView treeView = new TreeView();
        treeView.setId("0");
        List<TreeView> treeViews = this.authority_tree(treeView,role_pk);
        renderJson(JFinalJson.getJson().toJson(treeViews));
    }

    /**
     * 查询数据列表
     */
    public void list()
    {
//        String menu_pk = getPara("menu_pk");
        SysMenu sysMenu = getModel(SysMenu.class,"");
        List<SysMenu> sysMenus= menuService.findList(sysMenu);
        renderJson(JFinalJson.getJson().toJson(sysMenus));

    }

    /**
     * 保存方法
     */
    public void save()
    {
//        new SysStaff().set()
//        getModel()
        //前台页面的model名称，没有则为空，否则映射不到值
        AjaxMsg ajaxMsg = new AjaxMsg();
        boolean res = false;

        try {

            SysMenu menu = getModel(SysMenu.class,"");
            if (menu.getMenuPk() != null && !menu.getMenuPk().equals(""))
            {
                //更新方法
                res = menu.update();
            }
            else {
                //保存方法
                res = menu.save();
            }
            if(res)
            {
                ajaxMsg.setState("success");
                ajaxMsg.setMsg("保存成功");
            }
            else
            {
                ajaxMsg.setState("fail");
                ajaxMsg.setMsg("保存失败");
            }
//        getBean(Staff.class).save();
        }catch (Exception e)
        {
            e.getMessage();
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("保存失败");
        }
        renderJson(ajaxMsg);

    }

    /**
     * 使用递归查询列表页树形菜单
     * @param menu
     * @return
     */
    List<Menu> root = new ArrayList<Menu>();
    private List<Menu> tree(Menu menu)
    {

        Menu tmp_menu;
        List<Menu> menu_childs = new ArrayList<Menu>();
        List<SysMenu> sysmenu_parents= menuService.findChildList(menu);
        if (sysmenu_parents != null && sysmenu_parents.size()>0)
        {
            for (SysMenu sysmenu_parent:sysmenu_parents)
            {
                if (sysmenu_parent != null)
                {
                    tmp_menu = new Menu();
                    tmp_menu.setId(sysmenu_parent.getMenuPk());
                    tmp_menu.setName(sysmenu_parent.getMenuName());
                    tmp_menu.setMenu_parent(sysmenu_parent.getMenuParent());
                    tmp_menu.setMenu_icon(sysmenu_parent.getMenuIcon());
                    tmp_menu.setMenu_number(sysmenu_parent.getMenuNumber());
                    tmp_menu.setMenu_type(sysmenu_parent.getMenuType());
                    tmp_menu.setMenu_url(sysmenu_parent.getMenuUrl());
                    if (tmp_menu.getMenu_parent().equals("0"))
                    {
                        //递归调用之前先添加到全局list
                        root.add(tmp_menu);
                        //递归调用
                        this.tree(tmp_menu);
                    }
                    else
                    {
                        menu_childs.add(tmp_menu);
                        //递归调用
                        this.tree(tmp_menu);
                    }

                }
            }

        }
        menu.setChildren(menu_childs);
        return root;
    }


    /**
     * 使用递归查询登录后权限树形菜单
     * @param menu
     * @return
     */
    List<TreeView> treeViews = new ArrayList<TreeView>();
    private List<TreeView> authority_tree(TreeView menu,String role_pk)
    {

        TreeView tmp_menu;
        List<TreeView> menu_childs = new ArrayList<TreeView>();
        List<SysMenu> sysmenu_parents= menuService.findRolesMenu(menu,role_pk);
        if (sysmenu_parents != null && sysmenu_parents.size()>0)
        {
            for (SysMenu sysmenu_parent:sysmenu_parents)
            {
                if (sysmenu_parent != null)
                {
                    tmp_menu = new TreeView();
                    tmp_menu.setId(sysmenu_parent.getMenuPk().toString());
                    tmp_menu.setTitle(sysmenu_parent.getMenuName());
                    tmp_menu.setIcon(sysmenu_parent.getMenuIcon());
                    tmp_menu.setUrl(sysmenu_parent.getMenuUrl());
                    tmp_menu.setSpread(false);
                    if (sysmenu_parent.getMenuParent().equals("0"))
                    {
                        //递归调用之前先添加到全局list
                        treeViews.add(tmp_menu);
                        //递归调用
                        this.authority_tree(tmp_menu,role_pk);
                    }
                    else
                    {
                        menu_childs.add(tmp_menu);
                        //递归调用
                        this.authority_tree(tmp_menu,role_pk);
                    }

                }
            }

        }
        menu.setChildren(menu_childs);
        return treeViews;
    }

    /**
     * 授权页面弹出的树形列表
     */
    public void authority_ztree()
    {
        String role_pk = getPara("role_pk");
        Ztree ztree = null;
        List<Ztree>  ztreelist = new ArrayList<>();
        //已经授权过的列表查询
        List<Record> recordList=menuService.findCheckedMenuList(role_pk);
        if (recordList ==null||recordList.size()==0) {
            //未授权过的列表查询
            recordList=menuService.findList("");
        }

        for (Record record:recordList)
        {
            ztree = new Ztree();
            ztree.setId(record.getStr("menu_pk"));
            ztree.setName(record.getStr("menu_name"));
            if (record.getStr("checked")!=null&&record.getStr("checked").equals("1")) {
                ztree.setChecked(true);
            }
            else
            {
                ztree.setChecked(false);
            }
            ztree.setpId(record.getStr("menu_parent"));
            ztreelist.add(ztree);
        }
//        return ztreelist;
        renderJson(JFinalJson.getJson().toJson(ztreelist));
    }
}
